#!/usr/bin/env tarantool

--------------------------------------------------------------------------------
-- imports
--------------------------------------------------------------------------------

local clock = require "clock"
local http_router = require "http.router"
local http_server = require "http.server"
local log = require "log"
local uuid = require "uuid"

--------------------------------------------------------------------------------
-- constants
--------------------------------------------------------------------------------

-- rate limiter parameters
-- TODO: FIXME: any helper included in tarantool?
local THROTTLE_RATE = tonumber(os.getenv("THROTTLE_RATE")) or 2
local THROTTLE_CAPACITY = tonumber(os.getenv("THROTTLE_CAPACITY")) or 3

--------------------------------------------------------------------------------
-- setup database
--------------------------------------------------------------------------------

box.cfg {
  -- NB: exposed via HTTP
  -- listen = 3301
}

box.once("bootstrap", function()
  local space = box.schema.space.create("kv", {
    format = {
      { "key", "string" },
      { "value", "map" },
    }
  })
  space:create_index("primary", {
    type = "HASH", parts = { 1, "string" }
  })
  box.schema.user.create("u1", { password = "p1" })
  box.schema.user.grant("u1", "read,write,execute", "space", "kv")
end)

--------------------------------------------------------------------------------
-- API
--------------------------------------------------------------------------------

--
-- logger middleware
-- TODO: extract to lib
--
local function log_handler(req)
  req.logid = uuid.str()
  req.timestamp = clock.monotonic()

  local resp = req:next()

  local duration = clock.monotonic() - req.timestamp
  log.info(
    "KV> %s %s %s => %d (%.3fms)",
    req.logid,
    req:method(),
    req:path(),
    resp.status or 200, -- TODO: FIXME: req:render does not set resp.status?
    duration * 1000
  )

  return resp
end

--
-- leaky bucket request rate limiter middleware factory
-- TODO: FIXME: anything native exists?
-- TODO: extract to lib
--
local function make_throttle_handler(opts)
  local bucket = {
    timestamp = 0,
    value = 0
  }
  return function(req)
    local now = clock.monotonic()
    local allowed = false
    local v = 0
    if bucket.timestamp > 0 then
      local dt = now - bucket.timestamp
      v = bucket.value - opts.rate * dt
      if v < 0 then v = 0 end
    end
    if math.ceil(v) < opts.capacity then
      v = v + 1
      allowed = true
    end
    bucket.timestamp = now
    bucket.value = v
    if not allowed then
      return { status = 429 }
    end

    return req:next()
  end
end

--------------------------------------------------------------------------------
-- HTTP server
--------------------------------------------------------------------------------

local httpd = http_server.new("0.0.0.0", 48080, {
  log_requests = true,
  log_errors = true
})

local router = http_router.new()
httpd:set_router(router)

assert(
  router:use(make_throttle_handler({ capacity = THROTTLE_CAPACITY, rate = THROTTLE_RATE }), { }),
  "failed to register throttle_handler"
)
assert(router:use(log_handler, { }), "failed to register log_handler")

router:route({ method = "POST", path = "/kv" }, function(req)
  local ok, data = pcall(req.json, req)
  if ok == false then
    return { status = 400, body = "body must be valid json" }
  end
  local id = data.key
  if type(id) ~= "string" then
    return { status = 400, body = "body must provide key field" }
  end
  if type(data.value) ~= "table" then
    return { status = 400, body = "body must provide value field of type map" }
  end

  local result
  ok, result = pcall(
    box.space.kv.insert,
    box.space.kv,
    { data.key, data.value }
  )
  if ok == false then
    if result.code == box.error.TUPLE_FOUND then
      return { status = 409 }
    end
    if result.code == box.error.FIELD_TYPE then
      return { status = 400, body = "body must provide value field of type map" }
    end
    return { status = 500, body = result }
  end

  return { status = 201 }
end)

router:route({ method = "PUT", path = "/kv/:id" }, function(req)
  local ok, data = pcall(req.json, req)
  if ok == false then
    return { status = 400, body = "body must be valid json" }
  end
  if type(data.value) ~= "table" then
    return { status = 400, body = "body must provide value field of type map" }
  end

  local id = req:stash("id")
  local result
  ok, result = pcall(
    box.space.kv.update,
    box.space.kv,
    { id },
    { { "=", 2, data.value } }
  )
  if ok == false then
    if result.code == box.error.FIELD_TYPE then
      return { status = 400, body = "body must provide value field of type map" }
    end
    return { status = 500, body = result }
  end
  if result == nil then
    return { status = 404 }
  end

  return { status = 204 }
end)

router:route({ method = "GET", path = "/kv/:id" }, function(req)
  local id = req:stash("id")
  local result = box.space.kv:get { id }
  if result == nil then
    return { status = 404 }
  end

  return req:render { json = result.value }
end)

router:route({ method = "DELETE", path = "/kv/:id" }, function(req)
  local id = req:stash("id")
  local result = box.space.kv:delete { id }
  if result == nil then
    return { status = 404 }
  end

  return { status = 204 }
end)

--------------------------------------------------------------------------------

httpd:start()

--------------------------------------------------------------------------------
