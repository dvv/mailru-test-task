all:

start:
	docker-compose up -d

restart:
	docker-compose up -d --force-recreate

stop:
	docker-compose down

log logs:
	docker-compose logs -f

test:
	tarantool test/smoke-test.lua

.PHONY: all start restart stop log logs test
.SILENT:
