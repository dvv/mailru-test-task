#!/usr/bin/env tarantool

local test = require("tap").test("api")
-- test:plan(31)

local check_crud_raw, check_crud
do
  local ffi = require "ffi"
  ffi.cdef [[ unsigned int usleep(unsigned int seconds); ]]
  local json = require "json"
  local cli = require("http.client").new()
  local URL_KV = "http://127.0.0.1:48080/kv"
  local json_headers = {
    ["Content-Type"] = "application/json"
  }
  local crud = {
    create = function(key, value)
      return cli:request(
        "POST",
        URL_KV,
        json.encode({ key = key, value = value }),
        { headers = json_headers, timeout = 5 }
      )
    end,
    read = function(key)
      local res = cli:request(
        "GET",
        ("%s/%s"):format(URL_KV, key),
        nil,
        { timeout = 5 }
      )
      if res.body then
        local ok, body = pcall(json.decode, res.body)
        if ok then
          res.body = body
        end
      end
      return res
    end,
    update = function(key, value)
      return cli:request(
        "PUT",
        ("%s/%s"):format(URL_KV, key),
        json.encode({ value = value }),
        { headers = json_headers, timeout = 5 }
      )
    end,
    delete = function(key)
      return cli:request(
        "DELETE",
        ("%s/%s"):format(URL_KV, key),
        nil,
        { timeout = 5 }
      )
    end,
  }
  check_crud_raw = function(method, ...)
    local res = crud[method](...)
    return { res.status, res.body }
  end
  check_crud = function(method, ...)
    ffi.C.usleep(500000)
    return check_crud_raw(method, ...)
  end
end

do

  local key = "bebebe"

  test:is(check_crud("read", key)[1], 404, "can not read missing record")
  test:is(check_crud("update", key, { "bububu" })[1], 404, "can not update missing record")
  test:is(check_crud("update", key, nil)[1], 400, "can not update missing record with bad value")
  test:is(check_crud("delete", key)[1], 404, "can not delete missing record")

  test:is(check_crud("create", key, nil)[1], 400, "can not create with bad value")
  test:is(check_crud("create", key, { foo = 12345 })[1], 201, "create")
  test:is_deeply(check_crud("read", key), { 200, { foo = 12345 } }, "read created")
  test:is(check_crud("create", key, { foo = 12345 })[1], 409, "refuse to create existing record")
  test:is(check_crud("update", key, nil)[1], 400, "can not update with bad value")
  test:is(check_crud("update", key, { foo = "bububu" })[1], 204, "update")
  test:is_deeply(check_crud("read", key), { 200, { foo = "bububu" } }, "read updated")
  test:is(check_crud("delete", key)[1], 204, "delete")
  test:is(check_crud("read", key)[1], 404, "can not read deleted")

end

os.exit(test:check() and 0 or 1)
