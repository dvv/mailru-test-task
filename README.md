Test key-value storage in Tarantool
========================

See `COPYRIGHT` for the copyright information.

Installation:
-----------

Clone. Edit environment in `docker-compose.yml`.

Usage:
-----------

```sh
make start  # start server
make log    # follow server logs
make test   # run crud tests
make stop   # stop server
```

CRUD API is exposed at endpoint `http://127.0.0.1:48080/kv`.

### Create

```sh
curl http://127.0.0.1:48080/kv -H 'Content-Type: application/json' -d '{ "key": "foo", "value": { "foo": "bar", "bar": true } }'
```

### Read

```sh
curl http://127.0.0.1:48080/kv/foo
```

### Update

```sh
curl http://127.0.0.1:48080/kv/foo -H 'Content-Type: application/json' -X PUT -d '{ "value": { "foo": "BAR", "bar": false } }'
```

### Delete

```sh
curl http://127.0.0.1:48080/kv/foo -X DELETE
```

Tests:
-----------

Run `make test` in parallel to running server.
